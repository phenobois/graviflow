﻿Public Class GRAVIFLOW

    Dim RS232_read As String
    ' Dim time_ellapsed As Double
    Dim save_data_filename As String
    Dim header As String
    Dim data_to_save As String
    Dim instant_weight
    Dim delta__weight
    Dim flow_

    Dim boolean_measOK = False

    Dim Stopwatch1 As New Stopwatch
    Dim StopwatchElapsedMilliseconds
    Dim weight_string As String
    Dim sum_flow
    Dim specificconductance_kgm_1Mpa_1s_1 As Double
    Dim deltatP_unit As String

    Dim conductance_kgsMPA As Double 'conductance in kg.s-1.MPa-1
    Dim lastval As String = "(cm)"



    Dim myPort As Array  'COM Ports detected on the system will be stored here
    Delegate Sub SetTextCallback(ByVal [text] As String) 'Added to prevent threading errors during receiveing of data

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        '   cmbport.SelectedIndex = 2
        save_data_filename = "none"

        myPort = IO.Ports.SerialPort.GetPortNames() 'Get all com ports available
        cmbBaud.Items.Add(1200)     'Populate the cmbBaud Combo box to common baud rates used
        cmbBaud.Items.Add(9600)
        cmbBaud.Items.Add(19200)

        Try
            For i = 0 To UBound(myPort)
                cmbport.Items.Add(myPort(i))
            Next
            cmbport.Text = cmbport.Items.Item(0)    'Set cmbPort text to the first COM port detected
            cmbBaud.Text = cmbBaud.Items.Item(0)    'Set cmbBaud text to the first Baud rate on the list

            btnDisconnect.Enabled = False           'Initially Disconnect Button is Disabled



            With SerialPort1
                .PortName = cmbport.Text
                .BaudRate = 1200
                .DataBits = 7
                .StopBits = IO.Ports.StopBits.One
                .Parity = IO.Ports.Parity.Even
                ' .NewLine = Chr(13)



            End With


            ' open port com, if not already open
            'If SerialPort1.IsOpen = False Then
            '    SerialPort1.Open()
            'Else
            '    enable laser
            '    SerialPort1.RtsEnable = True

            '    RS232_read = SerialPort1.ReadLine
            '    RichTextBox1.Text = RS232_read



            'End If

        Catch
        End Try

        ComboBox_pressure_unit.SelectedIndex = 0

    End Sub




    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Try
            SerialPort1.PortName = cmbport.Text         'Set SerialPort1 to the selected COM port at startup
            SerialPort1.BaudRate = cmbBaud.Text         'Set Baud rate to the selected value on

            'Other Serial Port Property
            SerialPort1.Parity = IO.Ports.Parity.Even
            SerialPort1.StopBits = IO.Ports.StopBits.One
            SerialPort1.DataBits = 7
            '  SerialPort1.Handshake = IO.Ports.Handshake.XOnXOff

            'Open our serial port
            SerialPort1.Open()

            btnConnect.Enabled = False          'Disable Connect button
            btnDisconnect.Enabled = True        'and Enable Disconnect button

        Catch ex As Exception
            MsgBox("unable to connect")
        End Try

    End Sub

    Private Sub btnDisconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisconnect.Click
        SerialPort1.Close()             'Close our Serial Port

        btnConnect.Enabled = True
        btnDisconnect.Enabled = False
    End Sub

 

    Private Sub SerialPort1_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived

        If SerialPort1.IsOpen = True Then
            Try
                ReceivedText(SerialPort1.ReadLine())
            Catch
            End Try
        End If
        'Automatically called every time a data is received at the serialPort     
    End Sub
    Private Sub ReceivedText(ByVal [text] As String)
        'compares the ID of the creating Thread to the ID of the calling Thread

        If Me.RichTextBox1.InvokeRequired Then
            Dim x As New SetTextCallback(AddressOf ReceivedText)
            Me.Invoke(x, New Object() {(text)})

        Else
            Me.RichTextBox1.Text &= [text]
            lastval = [text]
            TextBox_rawweight.Text = lastval
        End If


    End Sub

    Private Sub cmbPort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbport.SelectedIndexChanged
        If SerialPort1.IsOpen = False Then
            SerialPort1.PortName = cmbport.Text         'pop a message box to user if he is changing ports
        Else                                                                               'without disconnecting first.
            MsgBox("Valid only if port is Closed", vbCritical)
        End If
    End Sub

    Private Sub cmbBaud_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBaud.SelectedIndexChanged
        If SerialPort1.IsOpen = False Then
            SerialPort1.BaudRate = cmbBaud.Text         'pop a message box to user if he is changing baud rate
        Else                                                                                'without disconnecting first.
            MsgBox("Valid only if port is Closed", vbCritical)
        End If
    End Sub



    Public Sub send_data_to_datagridview()


        'SAVED_data_list = measurement_date, TextBox_campaign_name.Text, sample_ref1, sample_ref2, sample_ref3, Cavispeed_rpm, max_Cavispeed_rpm, Format(max_centri_tension," & Chr(34) & "0.###" & Chr(34) & "), RichTextBox_mean_inst_LP_Cochard.Text, std_dev_lp_cochard, Format(PLC," & Chr(34) & "0.##" & Chr(34) & "), RichTextBox_mean_inst_LP_integral.Text, standard_deviation_integral, Format(PLC_integral," & Chr(34) & "0.##" & Chr(34) & "), "", speed_class, plate_IR_temp, cavi_Temp1, cavi_Temp2, correction_Temp, operator_name, ComboBox_plate_number.Text, species, lieu_prelevement, sample_type, treeage, TextBox_branch_diam.Text, delta_P, calib_optic_grid, meas_MODE
        ' DataGridView1.Rows.Add(TextBox_samplename.Text, time_interval, weight, delta_weight, Flow, mean_flow)
        Try
            Dim i As Int16



            instant_weight = ""
            For i = 0 To weight_string.Length - 1

                If IsNumeric(weight_string(i)) = True Or weight_string(i) = "." Or weight_string(i) = "-" Then  ' Or weight_string(i).IsPunctuation = True Then

                    instant_weight = instant_weight & weight_string(i)
                End If
            Next

            Dim instant_weight_double = Convert.ToDouble(instant_weight)

            If DataGridView1.RowCount > 1 Then
                If (instant_weight <> "") Then
                    If IsNumeric(DataGridView1.Rows(DataGridView1.RowCount - 2).Cells("weight").Value) Then
                        delta__weight = Format(instant_weight_double - DataGridView1.Rows(DataGridView1.RowCount - 2).Cells("weight").Value, "###.#####")
                    End If
                    StopwatchElapsedMilliseconds = Stopwatch1.ElapsedMilliseconds
                    flow_ = delta__weight / (StopwatchElapsedMilliseconds / 1000)


                End If
            Else
                delta__weight = 0

            End If





            ' COMPUTE MEAN CONDUCTANCE (kg.s-1.MPa-1 )

            Select Case ComboBox_pressure_unit.SelectedIndex
                Case 0  'unit is cm H2O
                    conductance_kgsMPA = flow_ / (TextBox_pressure_gradient.Text * 0.0000980665) / 1000
                Case 1 'unit is cm MPa
                    conductance_kgsMPA = flow_ / TextBox_pressure_gradient.Text / 1000
                Case 2 'unit is cm bar
                    conductance_kgsMPA = flow_ / (TextBox_pressure_gradient.Text / 10) / 1000
            End Select


            ' COMPUTE MEAN  SPECIFIC CONDUCTANCE (kg.m-1.s-1.MPa-1 )
            If TextBox_diameter.Text <> "" And TextBox_samplelength.Text <> "" Then

                specificconductance_kgm_1Mpa_1s_1 = conductance_kgsMPA / (Math.PI * (TextBox_diameter.Text / 1000 / 2) * (TextBox_diameter.Text / 1000 / 2)) * TextBox_samplelength.Text

            End If



            DataGridView1.Rows.Add(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), TextBox_samplename.Text, TextBox_diameter.Text, TextBox_samplelength.Text, TextBox_pressure_gradient.Text, StopwatchElapsedMilliseconds / 1000, instant_weight_double, delta__weight, flow_, conductance_kgsMPA, specificconductance_kgm_1Mpa_1s_1) ' delta_weight
            '  DataGridView1.Rows.Add(SAVED_data_list)
            save_data_to_file()

            DataGridView1.FirstDisplayedCell = DataGridView1(0, DataGridView1.Rows.Count - 1)

            Boolean_measOK = True
        Catch e As Exception
            '    MsgBox(e.Message, MsgBoxStyle.Exclamation)

        End Try

    End Sub

    Private Sub save_data_to_file()

        'chr(13) = ASCII codfe for line feed
        'chr(9) = ASCII code for tabulation



        If save_data_filename = "none" Then
            select_filename_save_data()
        Else

            header = "date_time,sample name,sample diameter (mm),sample length (m),Pressure gradient " & deltatP_unit & ",time interval(sec),weight (g),delta weight (g),flow (g/s),mean flow(g/s),mean conductance (kg.s-1.MPa-1),mean specific conductivity (kg.m-1.s-1.MPa-1 )" & Chr(13)


            My.Computer.FileSystem.WriteAllText(save_data_filename, header, False)

            data_to_save = ""

            For Each r As DataGridViewRow In Me.DataGridView1.Rows
                For Each c As DataGridViewCell In r.Cells
                    data_to_save = data_to_save & c.Value & ","
                Next
                data_to_save = data_to_save & Environment.NewLine
            Next


            My.Computer.FileSystem.WriteAllText(save_data_filename, data_to_save, True)
        End If

    End Sub

    Private Sub select_filename_save_data()
        '       MsgBox("Sélectionnez un nom de fichier pour l'enregistrement des données", MsgBoxStyle.OkOnly)

        SaveFileDialog1.DefaultExt = ".csv"
        SaveFileDialog1.FileName = TextBox_samplename.Text
        SaveFileDialog1.ShowDialog()

        save_data_filename = SaveFileDialog1.FileName



    End Sub




    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick



        ' lastval = RichTextBox1.Text.LastIndexOf("N")



        '    RS232_read = SerialPort1.ReadLine.StartsWith("N")

        weight_string = TextBox_rawweight.Text
   
        '  RichTextBox1.Text = RS232_read


        RichTextBox2.Text = RichTextBox2.Text + (Timer1.Interval / 1000)

        If save_data_filename <> "none" Then
      
            send_data_to_datagridview()
        Else
            MsgBox("Please select a file to save your data", MsgBoxStyle.Exclamation)
            Timer1.Stop()
            select_filename_save_data()

        End If

        If boolean_measOK = True Then
            Stopwatch1.Reset()
            Stopwatch1.Start()
            boolean_measOK = False

        End If


    End Sub

    Private Sub Button_startmeas_Click(sender As System.Object, e As System.EventArgs) Handles Button_startmeas.Click
        Timer1.Start()
        Button2.BackColor = Color.Green
        Stopwatch1.Start()
    End Sub


    Private Sub Buttonstop_Click(sender As System.Object, e As System.EventArgs) Handles Button_stop.Click
        Timer1.Stop()
        Button2.BackColor = Color.Red
        RichTextBox2.Text = 0
        Stopwatch1.Reset()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button_filename.Click
        select_filename_save_data()

        '    OpenFileDialog1.FileName

    End Sub

    Private Sub NumericUpDown1_ValueChanged(sender As System.Object, e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        Timer1.Interval = 1000 * NumericUpDown1.Value
    End Sub


   

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged

        If CheckBox1.Checked = True Then
            RichTextBox1.Visible = True
        Else
            RichTextBox1.Visible = False
        End If


    End Sub

    Private Sub RichTextBox1_TextChanged(sender As System.Object, e As System.EventArgs) Handles RichTextBox1.MouseClick
        Clipboard.SetDataObject(RichTextBox1.Text)
    End Sub

    
    Private Sub Button2_Click_1(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If Timer1.Enabled = True Then
            Timer1.Enabled = False
            Button2.BackColor = Color.Red
        Else
            Stopwatch1.Reset()
            Timer1.Enabled = True
            Button2.BackColor = Color.Green
        End If
    End Sub

   
    Private Sub ComboBox_pressure_unit_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox_pressure_unit.SelectedIndexChanged
        Select Case ComboBox_pressure_unit.SelectedIndex
            Case 0  'unit is cm H2O
                deltatP_unit = "(cm)"
            Case 1 'unit is  MPa
                deltatP_unit = "(MPa)"
            Case 2 'unit is  bar
                deltatP_unit = "(bar)"
        End Select
    End Sub

  
End Class
