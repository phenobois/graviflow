﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GRAVIFLOW
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.Button_startmeas = New System.Windows.Forms.Button()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.TextBox_rawweight = New System.Windows.Forms.TextBox()
        Me.cmbport = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBox_pressure_unit = New System.Windows.Forms.ComboBox()
        Me.TextBox_samplelength = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_diameter = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox_pressure_gradient = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox_samplename = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Button_filename = New System.Windows.Forms.Button()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.btnDisconnect = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbBaud = New System.Windows.Forms.ComboBox()
        Me.Button_stop = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.datetime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sample_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.diameter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.length = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pressure_gradient = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.time_interval = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.delta_weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Flow = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.instant_conductance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.instant_specific_conductivity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button_startmeas
        '
        Me.Button_startmeas.BackColor = System.Drawing.SystemColors.GrayText
        Me.Button_startmeas.Location = New System.Drawing.Point(316, 239)
        Me.Button_startmeas.Name = "Button_startmeas"
        Me.Button_startmeas.Size = New System.Drawing.Size(75, 48)
        Me.Button_startmeas.TabIndex = 0
        Me.Button_startmeas.Text = "start meas"
        Me.Button_startmeas.UseVisualStyleBackColor = False
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(3, 345)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(1169, 386)
        Me.RichTextBox1.TabIndex = 1
        Me.RichTextBox1.Text = ""
        Me.RichTextBox1.Visible = False
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.Location = New System.Drawing.Point(3, 56)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(257, 43)
        Me.RichTextBox2.TabIndex = 2
        Me.RichTextBox2.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(259, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 37)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "g"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(256, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 37)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "sec"
        '
        'Timer1
        '
        Me.Timer1.Interval = 10000
        '
        'SerialPort1
        '
        Me.SerialPort1.BaudRate = 1200
        Me.SerialPort1.DataBits = 7
        Me.SerialPort1.DiscardNull = True
        Me.SerialPort1.DtrEnable = True
        Me.SerialPort1.Parity = System.IO.Ports.Parity.Even
        Me.SerialPort1.ParityReplace = CType(0, Byte)
        Me.SerialPort1.PortName = "COM3"
        '
        'TextBox_rawweight
        '
        Me.TextBox_rawweight.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox_rawweight.Location = New System.Drawing.Point(3, 7)
        Me.TextBox_rawweight.Name = "TextBox_rawweight"
        Me.TextBox_rawweight.Size = New System.Drawing.Size(257, 43)
        Me.TextBox_rawweight.TabIndex = 5
        '
        'cmbport
        '
        Me.cmbport.FormattingEnabled = True
        Me.cmbport.Location = New System.Drawing.Point(104, 55)
        Me.cmbport.Name = "cmbport"
        Me.cmbport.Size = New System.Drawing.Size(69, 21)
        Me.cmbport.TabIndex = 7
        Me.cmbport.Text = "COM3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 15)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "com port name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox_pressure_unit)
        Me.GroupBox1.Controls.Add(Me.TextBox_samplelength)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox_diameter)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBox_pressure_gradient)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBox_samplename)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Location = New System.Drawing.Point(316, 42)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(306, 191)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "parameters"
        '
        'ComboBox_pressure_unit
        '
        Me.ComboBox_pressure_unit.FormattingEnabled = True
        Me.ComboBox_pressure_unit.Items.AddRange(New Object() {"cmH2O", "MPa", "bar"})
        Me.ComboBox_pressure_unit.Location = New System.Drawing.Point(215, 71)
        Me.ComboBox_pressure_unit.Name = "ComboBox_pressure_unit"
        Me.ComboBox_pressure_unit.Size = New System.Drawing.Size(78, 21)
        Me.ComboBox_pressure_unit.TabIndex = 26
        '
        'TextBox_samplelength
        '
        Me.TextBox_samplelength.Location = New System.Drawing.Point(200, 122)
        Me.TextBox_samplelength.Name = "TextBox_samplelength"
        Me.TextBox_samplelength.Size = New System.Drawing.Size(93, 20)
        Me.TextBox_samplelength.TabIndex = 25
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 15)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "branch length (m)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(133, 15)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "sample diameter (mm)"
        '
        'TextBox_diameter
        '
        Me.TextBox_diameter.Location = New System.Drawing.Point(199, 98)
        Me.TextBox_diameter.Name = "TextBox_diameter"
        Me.TextBox_diameter.Size = New System.Drawing.Size(94, 20)
        Me.TextBox_diameter.TabIndex = 22
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 72)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 15)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Pressure gradient"
        '
        'TextBox_pressure_gradient
        '
        Me.TextBox_pressure_gradient.Location = New System.Drawing.Point(116, 71)
        Me.TextBox_pressure_gradient.Name = "TextBox_pressure_gradient"
        Me.TextBox_pressure_gradient.Size = New System.Drawing.Size(93, 20)
        Me.TextBox_pressure_gradient.TabIndex = 20
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 15)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "sample name"
        '
        'TextBox_samplename
        '
        Me.TextBox_samplename.Location = New System.Drawing.Point(116, 44)
        Me.TextBox_samplename.Name = "TextBox_samplename"
        Me.TextBox_samplename.Size = New System.Drawing.Size(177, 20)
        Me.TextBox_samplename.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(123, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 15)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "timer interval (sec)"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(246, 13)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(47, 20)
        Me.NumericUpDown1.TabIndex = 9
        Me.NumericUpDown1.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Button_filename
        '
        Me.Button_filename.Location = New System.Drawing.Point(9, 216)
        Me.Button_filename.Name = "Button_filename"
        Me.Button_filename.Size = New System.Drawing.Size(75, 35)
        Me.Button_filename.TabIndex = 11
        Me.Button_filename.Text = "Select filename"
        Me.Button_filename.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(182, 55)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 39)
        Me.btnConnect.TabIndex = 15
        Me.btnConnect.Text = "connect port"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'btnDisconnect
        '
        Me.btnDisconnect.Location = New System.Drawing.Point(182, 14)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(75, 39)
        Me.btnDisconnect.TabIndex = 14
        Me.btnDisconnect.Text = "disconnect port"
        Me.btnDisconnect.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 15)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "baud rate"
        '
        'cmbBaud
        '
        Me.cmbBaud.FormattingEnabled = True
        Me.cmbBaud.Location = New System.Drawing.Point(104, 26)
        Me.cmbBaud.Name = "cmbBaud"
        Me.cmbBaud.Size = New System.Drawing.Size(69, 21)
        Me.cmbBaud.TabIndex = 12
        '
        'Button_stop
        '
        Me.Button_stop.BackColor = System.Drawing.SystemColors.GrayText
        Me.Button_stop.Location = New System.Drawing.Point(397, 239)
        Me.Button_stop.Name = "Button_stop"
        Me.Button_stop.Size = New System.Drawing.Size(75, 48)
        Me.Button_stop.TabIndex = 10
        Me.Button_stop.Text = "stop meas"
        Me.Button_stop.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.datetime, Me.sample_name, Me.diameter, Me.length, Me.pressure_gradient, Me.time_interval, Me.weight, Me.delta_weight, Me.Flow, Me.instant_conductance, Me.instant_specific_conductivity})
        Me.DataGridView1.Location = New System.Drawing.Point(3, 299)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1169, 432)
        Me.DataGridView1.TabIndex = 12
        '
        'datetime
        '
        Me.datetime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.datetime.HeaderText = "Date & time"
        Me.datetime.Name = "datetime"
        Me.datetime.Width = 120
        '
        'sample_name
        '
        Me.sample_name.HeaderText = "sample name"
        Me.sample_name.Name = "sample_name"
        '
        'diameter
        '
        Me.diameter.HeaderText = "sample diameter (mm)"
        Me.diameter.Name = "diameter"
        Me.diameter.Visible = False
        '
        'length
        '
        Me.length.HeaderText = "sample length m"
        Me.length.Name = "length"
        Me.length.Visible = False
        '
        'pressure_gradient
        '
        Me.pressure_gradient.HeaderText = "Pressure gradient"
        Me.pressure_gradient.Name = "pressure_gradient"
        '
        'time_interval
        '
        Me.time_interval.HeaderText = "time interval (sec)"
        Me.time_interval.Name = "time_interval"
        '
        'weight
        '
        Me.weight.HeaderText = "weight (g)"
        Me.weight.Name = "weight"
        '
        'delta_weight
        '
        Me.delta_weight.HeaderText = "Delta weight"
        Me.delta_weight.Name = "delta_weight"
        '
        'Flow
        '
        Me.Flow.HeaderText = "Flow (g/s)"
        Me.Flow.Name = "Flow"
        '
        'instant_conductance
        '
        Me.instant_conductance.HeaderText = "instant conductance (kg/s/MPa)"
        Me.instant_conductance.Name = "instant_conductance"
        '
        'instant_specific_conductivity
        '
        Me.instant_specific_conductivity.HeaderText = "instant specific conductivity (kg.m-1.Mpa-1.s-1)"
        Me.instant_specific_conductivity.Name = "instant_specific_conductivity"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cmbBaud)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.btnConnect)
        Me.GroupBox2.Controls.Add(Me.cmbport)
        Me.GroupBox2.Controls.Add(Me.btnDisconnect)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 113)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(288, 97)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "RS232 parameters"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(6, 74)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(156, 17)
        Me.CheckBox1.TabIndex = 23
        Me.CheckBox1.Text = "visualise raw RS232 values"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.GrayText
        Me.Button2.Location = New System.Drawing.Point(478, 239)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 48)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "PAUSE recording"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication1.My.Resources.Resources.Université_Bordeaux__Logo_2013_
        Me.PictureBox1.Location = New System.Drawing.Point(546, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(85, 33)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 15
        Me.PictureBox1.TabStop = False
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.SystemColors.Info
        Me.RichTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox3.Location = New System.Drawing.Point(325, 5)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.Size = New System.Drawing.Size(215, 31)
        Me.RichTextBox3.TabIndex = 16
        Me.RichTextBox3.Text = "Graviflow v1.1"
        '
        'Chart1
        '
        ChartArea2.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea2)
        Me.Chart1.Location = New System.Drawing.Point(661, 5)
        Me.Chart1.Name = "Chart1"
        Series2.ChartArea = "ChartArea1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series2.Name = "Series1"
        Me.Chart1.Series.Add(Series2)
        Me.Chart1.Size = New System.Drawing.Size(511, 288)
        Me.Chart1.TabIndex = 17
        Me.Chart1.Text = "Chart1"
        '
        'GRAVIFLOW
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1184, 743)
        Me.Controls.Add(Me.Chart1)
        Me.Controls.Add(Me.RichTextBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button_stop)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RichTextBox2)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.Button_startmeas)
        Me.Controls.Add(Me.TextBox_rawweight)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button_filename)
        Me.Name = "GRAVIFLOW"
        Me.ShowInTaskbar = False
        Me.Text = "GRAVIFLOW"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_startmeas As System.Windows.Forms.Button
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents TextBox_rawweight As System.Windows.Forms.TextBox
    Friend WithEvents cmbport As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button_stop As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button_filename As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbBaud As System.Windows.Forms.ComboBox
    Friend WithEvents btnDisconnect As System.Windows.Forms.Button
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox_samplename As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox_pressure_gradient As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox_samplelength As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox_diameter As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox_pressure_unit As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents datetime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sample_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents diameter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents length As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pressure_gradient As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents time_interval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents weight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents delta_weight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Flow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents instant_conductance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents instant_specific_conductivity As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
